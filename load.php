<?php
require('Autoloader.php');

$ESLoader = new SplClassLoader('EchoSign', realpath(__DIR__ . '/lib') . '/');
$ESLoader->register();