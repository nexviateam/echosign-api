<?php

namespace EchoSign\Options;

class GetDocumentImageUrlsOptions extends AbstractDocumentOptions
{

    protected $includeScalingTypes;

    function setIncludeScalingTypes($includeScalingTypes)
    {
        $this->includeScalingTypes = $includeScalingTypes;
    }

    function getIncludeScalingTypes()
    {
        return $this->includeScalingTypes;
    }

    function asArray()
    {
        $inherited = parent::asArray();

        $properties = array(
            'includeScalingTypes' => $this->includeScalingTypes
        );

        $properties = array_merge($inherited['options'], $properties);

        foreach ($properties as $k => $v) {
            if ($v === null || $v === '') {
                unset($properties[$k]);
            }
        }

        return array('options' => $properties);
    }
}